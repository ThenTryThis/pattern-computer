const example_code={

    halstattish:
`ieebebeb


dodod

`,
    Birka:
`;; Viking band circa 800 AD excavated from 
;; the Swedish island of Bj\u00f6rk\u00f6

(quick-warp 10 b g w w)
(warp-twist z z z z z z z z z z)
(warp-rotate 0 1 2 3 0 1 2 3 0 1) 

(repeat 6
    (twist 0 1 2 3 4 5 6)
    (weave-back 2)
    (twist 5 6)
    (weave-back 2)
    (weave-forward 2)
    (twist 3 4)
    (weave-forward 2)
    (twist 0 1 2))`,

    "Birka brocade":
    `;; This was the trim pattern most commonly 
;; represented in Birka's Viking Age burials.
;; Its popularity spanned the ninth and tenth 
;; centuries.
;; https://www.cs.vassar.edu/~capriest/birkarcp.html

(warp
 (b b b b) z ;; border
 (b w w w) z
 (w b w w) z
 (w b w w) s
 (b w w w) s
 (w w w b) s
 (w w b w) s
 (w b w w) s
 (b w w w) s
 (b w w w) z
 (w b w w) z
 (w b w w) s
 (b w w w) s
 (b b b b) z) ;; border

(weave-forward 1)

(repeat 5
  (weave-forward 2)
  (repeat 2 (weave f b b f f f f b b b b f f f))
  (repeat 2 (weave b f f f f b b b b f f f f b))
  (repeat 2 (weave b f f b b f f b b f f b b b)) 
  )`,
    
    "Egyptian river":
    `;; from "Byways in Handweaving" by Mary Meigs Atwater
(quick-warp 10 b w w b)
(warp-rotate 0 1 2 3 4 5 6 7 8 9)
(warp-twist z z z z z z z z z z)

(repeat 4
    (twist 0)
    (weave-forward 1)
    (twist 1)
    (weave-forward 1)
    (twist 2)
    (weave-forward 1)
    (twist 3)
    (weave-forward 1)
    (twist 4)
    (weave-forward 1)
    (twist 5)
    (weave-forward 1)
    (twist 6)
    (weave-forward 1)
    (twist 7)
    (weave-forward 1)
    (twist 8)
    (weave-forward 1)
    (twist 9)
    (weave-forward 1))`,

    "Running dog":
    `(quick-warp 10 w b b w)
(warp-twist z z z z z z z z z z)
(warp-rotate 0 1 2 3 0 1 2 3 0 1)

(repeat 6
    (twist 0 1 2 3)
    (weave-back 4)
    (twist 7 8 9)
    (weave-back 4))`,

    "Diagonal meander":
    `(quick-warp 10 b w w b)
(warp-twist z z z z z z z z z z)
(warp-rotate 0 1 2 3 0 1 2 3 0 1)

(repeat 6
    (twist 0 1 2 3)
    (weave-forward 4)
    (twist 7 8 9)
    (weave-forward 4))`,


    Waves:
`(warp
  (w w w w) s 
  (w w w b) s
  (w w b y) s
  (w b y b) s
  (b y b w) s
  (y b w b) s
  (y b w b) z
  (y y b w) z
  (y y y b) z
  (y y y y) z
)

(weave-forward 3)

(repeat 4
  (repeat 4 (weave f b b b f f b b b f))
  (weave-forward 4))`,


    "Workshop diamonds":
`;; this pattern is compatible with the
;; thread warping of the workshop tablets
(quick-warp 10 g w b w)
(warp-rotate 0 1 2 3 0 0 3 2 1 0)
(twist 0 2 4 5 7 9)

(repeat 2
    (weave-forward 10)
    (weave-back 10))`,

    "Workshop river":
`;; this pattern is compatible with the
;; thread warping of the workshop tablets
(quick-warp 10 g w b w)
(warp-rotate 0 1 2 3 0 1 2 3 0 1)
(twist 0 2 4 6 8)

(repeat 2
    (weave-forward 10)
    (weave-back 10))`,


    "Workshop raver":
`;; this pattern is compatible with the
;; thread warping of the workshop tablets

(quick-warp 10 g w b w)

;; we can use twists to show/hide colours
(repeat 4
    (twist 0 3 4 5 6 9)
    (weave-forward 2)
    (weave-back 2)
    (twist 3 6)
    (weave-forward 2)
    (twist 1 2 3 6 7 8)
    (weave-back 2)
    (twist 0 1 8 9)
    (weave-forward 2)
    (weave-back 2))`,

    "Hallstatt section":
    `;; Ancient pattern from the Iron Age salt-mines
;; of Hallstatt, circa 800 - 400 BC

(warp 
  (y y g g) s
  (y g g y) s
  (y y g g) s
  (y g g y) s
  (g g y y) s
  (g y y g) s
  (y y g g) s ;; --
  (g y y g) s
  (g g y y) s 
  (g y y g) s
  (y y g g) s
  (g y y g) s
  (y y g g) s
)

(weave-back 1)

;; Squint and you can see the underlying logic
(repeat 3
    (weave f f b b b b f f b b b f f f)
    (weave f f b b b f f b b b b f f f)
    (weave b f b b f f b b b b b f f f)
    (weave f b b f f b b b b b b f f f)
    (weave b b f f b b b b b b b f f f)
    (weave b f f b b f f f b b b f f f)
    (weave f f b b b f f f b b b f f f)
    (weave f b b b b f f f b b b b b f)
    (weave b b b b b f f f b b b b b f)
    (weave b b b b b f f f b b b b b f) 
    (weave b b b b b f f f b b b b f f)
    (weave f f b b b f f f b b b f f f)
    (weave f f b b b f f f b b f f b f)
    (weave f f b b b b b b b f f b b f)
    (weave f f b b b b b b f f b b f f)
    (weave f f b b b b b f f b b f b f) 
)`,

    "Snartemo II":
    `;; The Tablet Woven Band from the Viking
;; Snartemo II Grave
;; https://www.shelaghlewins.com/tablet_weaving/patterns_past.php

(quick-warp 12 r b b y)
(warp-rotate 0 1 2 2 1 0 0 1 2 2 1 0)
(warp-twist z z z s s s z z z s s s)

(weave-forward 12)
(weave-back 12)
(weave-forward 12)
(weave-back 12)`,



    "Oseberg Band":
`;; From the Oseberg ship burial, Vestfold, Norway
;; Made shortly before 834 AD
;; https://www.shelaghlewins.com/tablet_weaving/patterns_past.php

(warp
    (y y y y) z
    (y y y y) s
    (y y y y) z
    (b b b b) s
    (y y y b) z
    (b y b y) z
    (y b y y) z
    (b b b b) s
    (y y y y) z
    (y y y y) s
)

(weave-forward 24)

(rotate-back 4)
(rotate-back 4)

(weave-forward 24)`

    
}


// (warp-tablets 10 g w b w)

// (repeat 7
//     (weave-forward 2)
//     (weave-back 2)
//     (twist 1 4 5 8)
//     (weave-forward 2)
//     (twist 3 6)
//     (weave-back 2)
//     (twist 2 7))

